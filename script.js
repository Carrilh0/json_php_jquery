
function carregarItens(){

    var itens = ""; 
    var url ="dados.php";

    $.ajax({
        url: url,
        cache: false,
        dataType: "json",
        beforeSend: function(){
            $("h2").html("Carregando...");
        },
        error: function(){
            $("h2").html("Há algum problema com a fonte de dados");
        },
        
        success: function(retorno){     //retorno == json 
            
            if(retorno[0].erro){
                $("h2").html(retorno[0].erro);
            }else{
                
                    //laço para criar linhas da tabela
                for(var i = 0; i < retorno.length; i++){
                    itens += "<tr>";
                    itens += "<td>" + retorno[i].id + "</td>";
                    itens += "<td>" + retorno[i].nome + "</td>";
                    itens += "<td>" + retorno[i].console + "</td>";
                    itens += "<td>" + retorno[i].preco + "</td>";
                    itens += "</tr>";
                }
                //Preencher a Tabela
                $("#minhaTabela tbody").html(itens);

                //Limpar Statys de Carregando;
                $("h2").html("Carregado");
            }
        }
    });
}