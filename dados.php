<?php

header("Content-Type: text/plain");

$host = '127.0.0.1'; // Ip do banco
$login = 'phpmyadmin'; // Usuário
$senha = '5428'; //  Senha
$db_name = 'json'; // Banco
$con = null; // conexão

$con = mysqli_connect ($host, $login, $senha, $db_name);

if(!$con){
    echo '[{"erro: Não foi possivel conectar ao banco"';
    echo '}]';
}else{
    //SQL de BUSCA LISTAGEM
    $sql = "SELECT * FROM jogos";
    $result = mysqli_query($con,$sql); //executa o SQL
    $n = mysqli_num_rows($result); //Numero de linhas retornadas
    
    if(!$result){
        //Caso não haja resultado
        echo '[{"erro": "Há algum erro com a busca. Não retrona resultado"}]';
    }else if ($n<1){
        echo '[{"erro": "Não há nenhum dado cadastrado"}]';
    }else{
        //Mesclar resultado em um array
        for ($i = 0; $i<$n; $i++){
            $dados [$i] = mysqli_fetch_assoc($result);
        }

        echo json_encode($dados, JSON_PRETTY_PRINT);

    }
}

?>