<html lang="pt-br">
    <head>
    
        <meta charset="utf-8">
        <title> Tutorial Json </title>
        <!-- BootStrap -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <!-- JQuery -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <!-- Script -->
        <script src="script.js"></script>
    </head>
    
    <body onload="carregarItens()">
    
        <section>
            <center><h1>Tutorial Json</h1></center>
       
        <h2></h2>
        <!-- Tabela -->
        
            <table class="table table-striped" id="minhaTabela">
                <thead>
                    <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Jogo</th>
                    <th scope="col">Console</th>
                    <th scope="col">Valor</th>
                    </tr>
                </thead>
                <tbody>
                    
                </tbody>
            </table>
        
        </section>

    </body>

</html>